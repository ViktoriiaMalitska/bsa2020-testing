import CartParser from './CartParser';

let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe("CartParser - unit tests", () => {
	// Add your unit tests here.
  
	it("parseLine returns an object with keys from column keys and values from CSV", () => {
	  expect(parser.parseLine("Condimentum aliquet,13.90,1")).toEqual({
		id: expect.any(String),
		name: "Condimentum aliquet",
		price: 13.9,
		quantity: 1,
	  });
	});
  
	it("validate 'header' equal to the schema columns name returns an empty [] validation is successful", () => {
	  expect(
		parser.validate(`Product name,Price,Quantity\ \nTvoluptatem,10.32,1`)
	  ).toEqual([]);
	});
  
	it("validate 'header' not equal to the schema columns name returns not empty [] of validation errors type:header", () => {
	  const testArr = [
		{
		  column: 0,
		  message:
			'Expected header to be named "Product name" but received Product.',
		  row: 0,
		  type: "header",
		},
	  ];
	  expect(parser.validate(`Product,Price,Quantity`)).toEqual(testArr);
	});
  
	it("validate row cells < schema  returns not empty [] of validation errors type:row ", () => {
	  const testArr = [
		{
		  column: -1,
		  message: "Expected row to have 3 cells but received 2.",
		  row: 1,
		  type: "row",
		},
	  ];
	  expect(
		parser.validate(`Product name,Price,Quantity\ \nTvoluptatem,10.32`)
	  ).toEqual(testArr);
	});
  
	it("validate some row cells is empty returns not empty [] of validation errors type:cell", () => {
	  const testArr = [
		{
		  column: 1,
		  message: "Expected cell to be a positive number but received \"\"\"\".",
		  row: 1,
		  type: "cell",
		},
	  ];
	  expect(
		parser.validate(`Product name,Price,Quantity\ \nTvoluptatem,"",5`)
	  ).toEqual(testArr);
	});
  
	it("validate some row number cells < 0 returns not empty [] of validation errors type:cell", () => {
	  const testArr = [
		{
		  column: 1,
		  message: "Expected cell to be a positive number but received \"-10\".",
		  row: 1,
		  type: "cell",
		},
	  ];
	  expect(
		parser.validate(`Product name,Price,Quantity\ \nTvoluptatem,-10,5`)
	  ).toEqual(testArr);
	});
  
	it("validate some row number !number returns not empty [] of validation errors type:cell", () => {
	  const testArr = [
		{
		  column: 1,
		  message: "Expected cell to be a positive number but received \"string\".",
		  row: 1,
		  type: "cell",
		},
	  ];
	  expect(
		parser.validate(`Product name,Price,Quantity\ \nTvoluptatem,string,5`)
	  ).toEqual(testArr);
	});
  
	it("validate some row number undefined returns not empty [] of validation errors type:cell", () => {
	  const testArr = [
		{
		  column: 1,
		  message: "Expected cell to be a positive number but received \"undefined\".",
		  row: 1,
		  type: "cell",
		},
	  ];
	  expect(
		parser.validate(`Product name,Price,Quantity\ \nTvoluptatem,undefined,5`)
	  ).toEqual(testArr);
	});
  });
  
  
  
  describe("CartParser - integration test", () => {
	it("parse returns needed JSON structure", () => {
	  expect(
		parser.parse(
		  `C:/Users/Viktoriia/Documents/GitHub/BSA2020-Testing/samples/cart.csv`
		)).toMatchObject({
		  items: expect.any(Array),
		  total: expect.any(Number),
	  });
	});
  });